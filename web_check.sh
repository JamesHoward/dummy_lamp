#!/bin/bash

sleep 10
apache2 -v
ps aux
URL_TO_CHECK=http://localhost
TEXT_TO_CHECK='Apache2 Ubuntu Default Page'

echo Testing for existence of ${URL_TO_CHECK} for ${TEXT_TO_CHECK}

IP=$(curl -s ${URL_TO_CHECK})

echo ${IP}
if [[ ${IP} == *"${TEXT_TO_CHECK}"* ]]
then
    echo "Found expected text in ${URL_TO_CHECK}!"
else
    echo "Could not find expected text ${URL_TO_CHECK} - Exit Build"
    exit 1
fi
